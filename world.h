/*
 * world.h
 *
 *  Created on: Mar 4, 2015
 *      Author: frank
 */

#ifndef WORLD_H_
#define WORLD_H_

#include "stdint.h"

/* world area dimensions */
/* map bit array of 32 x 32 onto array of 128 bytes*/
/* Actually, use 44 x 44, after this we start to run into memory shortage */
#define WORLD_WIDTH 44
#define WORLD_HEIGHT 44
#define WORLD_BYTES (WORLD_WIDTH * WORLD_HEIGHT) / 8

void set_bit(uint8_t * const world, uint8_t x, uint8_t y);
void clear_bit(uint8_t * const world, uint8_t x, uint8_t y);
uint8_t test_bit(const uint8_t * const world, uint8_t x, uint8_t y);
void clear_world(uint8_t * const world);
uint8_t count_neighbors(const uint8_t * const world, uint8_t x, uint8_t y);
void init_worlds();
void evolve_world(const uint8_t * const world_current, uint8_t * const world_next);
void copy_world(uint8_t * const world_to, const uint8_t * const world_from);

#endif /* WORLD_H_ */
