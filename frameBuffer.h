/*
 * framebuffer header
 */

#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include <stdint.h>
#include <avr/pgmspace.h>
#include "ssd1306.h"

void fb_draw_bitmap(uint8_t *buffer, const uint8_t * const bitmap, uint8_t height, uint8_t width, uint8_t pos_x, uint8_t pos_y);
void fb_draw_bitmap_font8(uint8_t column, uint8_t page, const uint8_t * const progmem_bitmap, uint8_t width, uint8_t *buff);
//void fb_draw_buffer(const uint8_t * const buffer);
void fb_set_pixel(uint8_t * const buffer, uint8_t pos_x, uint8_t pos_y, uint8_t pixel_status);
void fb_draw_pixel(uint8_t * const buffer, uint8_t pos_x, uint8_t pos_y);
void fb_draw_vline(uint8_t * const buffer, uint8_t x, uint8_t y, uint8_t length);
void fb_draw_hLine(uint8_t * const buffer, uint8_t pos_y, uint8_t start, uint8_t length);
void fb_draw_rectangle(uint8_t * const buffer, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void fb_draw_rectangle_fill(uint8_t * const buffer, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t fill);
void fb_invert(uint8_t status);
void fb_clear();
void fb_fill(uint8_t * const buffer, uint8_t fill);
void fb_memset_fill(uint8_t * const buffer, uint8_t fill);
void fb_show(const uint8_t * const buffer);
void fb_draw_line(uint8_t * const buff, int16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
void fb_draw_bitmap_font8_letter(uint8_t column, uint8_t page, uint8_t letter, const uint8_t * const progmem_bitmap,
        uint8_t width, uint8_t * const buff);
//void fb_draw_bitmap_font8_string(uint8_t column, uint8_t page, char *string, const uint8_t *progmem_bitmap,
//        uint8_t width, uint8_t *buff);
void fb_draw_bitmap_font8_string(uint8_t column, uint8_t page, const char * const string, const uint8_t * const progmem_bitmap,
        uint8_t width, uint8_t * const buff);
//private:
//    uint8_t buffer[1024];
//    SSD1306 oled;
//};

#endif /* FRAMEBUFFER_H_ */
