/*
 * world.c
 *
 *  Created on: Mar 4, 2015
 *      Author: frank
 */

#include "world.h"
#include <string.h>

/* define our worlds */
uint8_t world_current_gen[WORLD_BYTES];
uint8_t world_next_gen[WORLD_BYTES];

void set_bit(uint8_t * const world, uint8_t x, uint8_t y) {
    if ((x < WORLD_WIDTH) && (y < WORLD_HEIGHT)) {
        uint16_t k = ((uint16_t) y * WORLD_WIDTH) + x;
        //world[k / 8] |= (1 << (k % 8));
        world[k / 8] |= (0x80 >> (k % 8));
    }
}

void clear_bit(uint8_t * const world, uint8_t x, uint8_t y) {
    if ((x < WORLD_WIDTH) && ((uint16_t) y < WORLD_HEIGHT)) {
        uint16_t k = (y * WORLD_WIDTH) + x;
        //world[k / 8] |= (0x80 >> (k % 8));
        world[k / 8] &= ~(0x80 >> (k % 8));
    }
}

uint8_t test_bit(const uint8_t * const world, uint8_t x, uint8_t y) {
    if ((x < WORLD_WIDTH) && (y < WORLD_HEIGHT)) {
        uint16_t k = ((uint16_t) y * WORLD_WIDTH) + x;
        return (world[k / 8] & (0x80 >> (k % 8))) > 0;
    } else {
        return 2;
    }
}

/* dangerous */
uint8_t count_neighbors(const uint8_t * const world, uint8_t x, uint8_t y) {
    uint8_t cnt = 0;
    cnt += test_bit(world, x - 1, y - 1);
    cnt += test_bit(world, x, y - 1);
    cnt += test_bit(world, x + 1, y - 1);

    cnt += test_bit(world, x - 1, y);
    cnt += test_bit(world, x + 1, y);

    cnt += test_bit(world, x - 1, y + 1);
    cnt += test_bit(world, x, y + 1);
    cnt += test_bit(world, x + 1, y + 1);
    return cnt;

}

/* safer */
uint8_t count_wrapped_neighbors(const uint8_t * const world, uint8_t x, uint8_t y) {
    uint8_t cnt = 0;

    cnt += test_bit(world, (x + WORLD_WIDTH - 1) % WORLD_WIDTH, (y + WORLD_HEIGHT - 1) % WORLD_HEIGHT);
    cnt += test_bit(world, x, (y + WORLD_HEIGHT - 1) % WORLD_HEIGHT);
    cnt += test_bit(world, (x + 1) % WORLD_WIDTH, (y + WORLD_HEIGHT - 1) % WORLD_HEIGHT);

    cnt += test_bit(world, (x + WORLD_WIDTH - 1) % WORLD_WIDTH, y);
    cnt += test_bit(world, (x + 1) % WORLD_WIDTH, y);

    cnt += test_bit(world, (x + WORLD_WIDTH - 1) % WORLD_WIDTH, (y + 1) % WORLD_HEIGHT);
    cnt += test_bit(world, x, (y + 1) % WORLD_HEIGHT);
    cnt += test_bit(world, (x + 1) % WORLD_WIDTH, (y + 1) % WORLD_HEIGHT);
    return cnt;

}
void clear_world(uint8_t * const world) {
    memset(world, 0, WORLD_BYTES);
}

void init_worlds() {
    clear_world(world_current_gen);
    clear_world(world_next_gen);
}

uint8_t apply_rules(uint8_t * const world, uint8_t x, uint8_t y) {
    uint8_t cnt = count_neighbors(world, x, y);
    //uint8_t cnt = count_wrapped_neighbors(world_current, x, y);

    uint8_t current_cell_state = test_bit(world, x, y);

    if (cnt == 2) {
        return current_cell_state;
    } else if (cnt == 3) {
        return 1;
    }
    return 0;
}

/**
 * Take current world and evolve to new world
 *
 */
void evolve_world(const uint8_t * const world_current, uint8_t * const world_next) {
    clear_world(world_next);
    for (uint8_t x = 0; x < WORLD_WIDTH; x++) {
        for (uint8_t y = 0; y < WORLD_HEIGHT; y++) {
            uint8_t cnt = count_wrapped_neighbors(world_current, x, y);
            uint8_t current_cell_state = test_bit(world_current, x, y);

            // FIXME: not efficient
            if (current_cell_state) {
                if (cnt < 2) {
                    clear_bit(world_next, x, y);
                } else if (cnt == 2) {
                    set_bit(world_next, x, y);
                } else if (cnt == 3) {
                    set_bit(world_next, x, y);
                } else if (cnt > 3) {
                    clear_bit(world_next, x, y);
                }
            } else {
                if (cnt == 3) {
                    set_bit(world_next, x, y);
                } else {
                    clear_bit(world_next, x, y);
                }
            }
        }
    }
}

/**
 * Copy from world to world, for world sizes < 256 bytes
 */
void copy_world(uint8_t * const world_to, const uint8_t * const world_from) {
    for (uint8_t i = 0; i < WORLD_BYTES; i++) {
        world_to[i] = world_from[i];
    }
}
