PRG            = main
OBJ            = main.o uart.o twimaster.o ssd1306.o frameBuffer.o world.o
MCU_TARGET     = atmega32
OPTIMIZE       = -Os

DEFS           =
LIBS           =

# You should not have to change anything below here.

CC             = avr-gcc

CFLAGS        = -g -Wall $(OPTIMIZE) -mmcu=$(MCU_TARGET) $(DEFS) -std=gnu99
LDFLAGS       = -Wl,-Map,$(PRG).map
#LDFLAGS       = -Wl,-u,vfprintf -lprintf_flt -lm 

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump

AVRDUDE        = /usr/local/bin/avrdude
PORT	       = /dev/cu.usbserial-A9AP9V7V

#all: $(PRG).elf lst text eeprom
all: $(PRG).elf lst text

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -rf *.o $(PRG).elf *.eps *.png *.pdf *.bak
	rm -rf *.lst *.map $(EXTRA_CLEAN_FILES)


dl:
	# download, fuse external clock (16MHZ from stk500)
	#/usr/bin/avrdude -v -v -c stk500v2 -p atmega32 -P /dev/ttyUSB0   -U flash:w:$(PRG).hex -U lfuse:w:0xe0:m
	# download, fuse internal RC clock (8MHZ)
	#/usr/bin/avrdude -v -v -c stk500v2 -p atmega32 -P /dev/ttyUSB0   -U flash:w:$(PRG).hex -U lfuse:w:0xe4:m
	$(AVRDUDE) -v -v -c stk500v2 -p $(MCU_TARGET) -P $(PORT)  -U flash:w:$(PRG).hex -U lfuse:w:0xe4:m

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

# Rules for building the .text rom images

text: hex bin srec

hex:  $(PRG).hex
bin:  $(PRG).bin
srec: $(PRG).srec

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

# Rules for building the .eeprom rom images

eeprom: ehex ebin esrec

ehex:  $(PRG)_eeprom.hex
ebin:  $(PRG)_eeprom.bin
esrec: $(PRG)_eeprom.srec

%_eeprom.hex: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O ihex $< $@

%_eeprom.srec: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O srec $< $@

%_eeprom.bin: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O binary $< $@

# Every thing below here is used by avr-libc's build system and can be ignored
# by the casual user.

JPEGFILES               = main-setup.jpg

JPEG2PNM                = jpegtopnm
PNM2EPS                 = pnmtops
JPEGRESOLUTION          = 180
EXTRA_CLEAN_FILES       = *.hex *.bin *.srec *.eps

dox: ${JPEGFILES:.jpg=.eps}

%.eps: %.jpg
	$(JPEG2PNM) $< |\
	$(PNM2EPS) -noturn -dpi $(JPEGRESOLUTION) -equalpixels \
	> $@
