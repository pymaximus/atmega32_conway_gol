/****************************************************************************
 Title:    Implementation of Conway's game of life on SSD1306 Oled demo using I2C
 Author:   Frank Singleton
 File:     main.c
 Software: AVR-GCC 3.3
 Hardware: Atmega32 at 8 Mhz (Internal RC Osc). STK500v2

 Description:
 This example  runs Conway's game of life  and displays via an I2C SSD1306 OLED display.

 *****************************************************************************/

#include "defines.h"    // Always First !!
#include "i2cmaster.h"
#include "myFont.h"
#include "myBitMaps.h"
#include "ssd1306.h"
#include "frameBuffer.h"
#include "world.h"

#include <string.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//#define DevSSD1306  0x78      // device address of SSD1306 OLED, uses 8-bit address (0x3c)!!!

#define N_ELEMENTS(array) (sizeof(array)/sizeof((array)[0]))

extern uint8_t world_current_gen[];
extern uint8_t world_next_gen[];

uint8_t generation;
uint8_t pattern_index;

/* The memory buffer for the LCD */
static uint8_t buffer[SSD1306_BUFFERSIZE];

/* general buffer for copying from progmem to sram */
char str_buffer[30];

/*
 * Messages stored in PROGMEM
 */
const char string_1[] PROGMEM = "Game of Life";
const char string_2[] PROGMEM = "Game of Life";
const char string_3[] PROGMEM = "String 3";
const char string_4[] PROGMEM = "String 4";
const char string_5[] PROGMEM = "String 5";

//PGM_P const string_table[] PROGMEM = {
/*
 * string table holds pointers to all messages
 */
const char * const string_table[] PROGMEM = { string_1, string_2, string_3, string_4, string_5 };

/**
 * Copies world to frame buffer at x,y offset
 */
void copy_world_to_fb(const uint8_t * const world, uint8_t x_offset, uint8_t y_offset) {
    for (uint8_t x = 0; x < WORLD_WIDTH; x++) {
        for (uint8_t y = 0; y < WORLD_HEIGHT; y++) {
            fb_set_pixel(buffer, x + x_offset, y + y_offset, test_bit(world, x, y));
        }
    }
}

void set_world_state_pattern1(uint8_t * const world) {
    /* seed with 2 parallel lines, vertical */
    for (uint8_t i = 2; i < WORLD_HEIGHT - 2; i++) {
        set_bit(world, WORLD_WIDTH / 2, i);
        set_bit(world, WORLD_WIDTH / 2 + 1, i);
    }
}

void set_world_state_pattern2(uint8_t * const world) {
    /* seed with line, horizontal */
    for (uint8_t i = 2; i < WORLD_WIDTH - 2; i++) {
        set_bit(world, i, WORLD_HEIGHT / 2);
    }
}

void set_world_state_pattern3(uint8_t * const world) {
    /* seed with glider */
    set_bit(world, 2, 5);
    set_bit(world, 3, 5);
    set_bit(world, 4, 5);
    set_bit(world, 4, 4);
    set_bit(world, 3, 3);
}

void set_world_state_pattern4(uint8_t * const world) {
    /* seed with 2 line, horizontal */
    for (uint8_t i = 2; i < WORLD_WIDTH - 2; i++) {
        set_bit(world, i, WORLD_HEIGHT / 2 - 1);
        set_bit(world, i, WORLD_HEIGHT / 2);
    }
}

void set_world_state_pattern5(uint8_t * const world) {
    /* seed with 2 diagonal lines and 2 horizontal */
    for (uint8_t i = 0; i < WORLD_WIDTH; i++) {
        set_bit(world, i, i);
        set_bit(world, i, WORLD_HEIGHT - i - 1);
        set_bit(world, i, 0);
        set_bit(world, i, WORLD_HEIGHT - 1);
    }
}

/*
 * declare and populate function pointer array so we can chose
 * random starting patterns
 */
typedef void (*fp_t)(uint8_t * const world);
fp_t const function_array[] = { set_world_state_pattern1, set_world_state_pattern2, set_world_state_pattern4,
        set_world_state_pattern5 };

void set_initial_state() {
    fb_clear(buffer);
    /*
     * Copy title from progmem into sram buffer and display
     */
    strcpy_P(str_buffer, (char*) pgm_read_word(&(string_table[0])));
    fb_draw_bitmap_font8_string(0, 0, str_buffer, &(Font5x8[0][0]), 5, buffer);

    /* clear worlds and set initial state*/
    init_worlds();

    /* step to next pattern */
    pattern_index = (pattern_index + 1) % N_ELEMENTS(function_array);
    function_array[pattern_index](world_current_gen);

    /* copy world to frame buffer and display */
    copy_world_to_fb(world_current_gen, 48, 16);

    /* display generation number */
    utoa(generation, str_buffer, 10);
    fb_draw_bitmap_font8_string(0, 7, str_buffer, &(Font5x8[0][0]), 5, buffer);

    /* display world on oled */
    fb_show(buffer);

    /* for debug */
    PORTB = (1 << PB5);
    _delay_ms(5000);

}
/*
 * Main entry point
 */
int main(void) {
    cli();
    unsigned char ret;
    uint16_t generation = 0;

    /* set PORTB output, state high */
    DDRB = 0xff;
    PORTB = 0xff;

    _delay_ms(2000);

    /* init I2C bus interface */
    i2c_init();
    /* set device address and write mode */
    ret = i2c_start(SSD1306_DEFAULT_ADDRESS + I2C_WRITE);
    if (ret) {
        /* failed to issue start condition, possibly no device found */
        i2c_stop();
        /* activate all 8 LED to show error */
        PORTB = 0x00;
        _delay_ms(5000);
    } else {
        /* issuing start condition ok, device accessible */

        /* i2c ok, so init oled */
        oled_init();

        _delay_ms(5000);

        /* clear frame buffer */
        fb_clear(buffer);

    }

    /* initial state of simulation */
    set_initial_state();

    /* evolve forever */
    for (;;) {
        PORTB ^= (1 << PB5); /* indicate looping */
        evolve_world(world_current_gen, world_next_gen);
        copy_world_to_fb(world_next_gen, 48, 16);

        generation++;
        utoa(generation, str_buffer, 10);
        fb_draw_bitmap_font8_string(0, 7, str_buffer, &(Font5x8[0][0]), 5, buffer);

        fb_show(buffer);
        //_delay_ms(10000);
        copy_world(world_current_gen, world_next_gen);
        clear_world(world_next_gen);

        /* restart after 150 generations */
        if (generation > 149) {
            generation = 0;
            /* pause on last generation */
            _delay_ms(5000);
            set_initial_state();
        }
    }

}

