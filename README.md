atmega32 oled game of life
==========================

Tests AVR GCC workflow on Fedora 21

Target - atmega32 on STK500, 8MHz internal RC clock

Connected via I2C to
0.96" Inch Yellow and Blue I2C IIC Serial 128X64 OLED LCD LED Display Module for Arduino 51 MSP420 STIM32 SCR

Execute the following

    make clean
    make
    make dl

Atmega will run through 5 different seeds and display 100
generations of cellular evolution.

Target - UNO R3, 16MHz clock

UNO R3 branch also supports UNO R3 board, awesome !!

Target - Mega 2560 R3, 16MHz clock

Mega 2560R3 branch also supports Mega 2560 R3 board, awesome !!


Example running on Arduino Uno R3
---------------------------------


![Screenshot](images/image_gol_1.jpg)

![Screenshot](images/image_gol_2.jpg)

![Screenshot](images/image_gol_3.jpg)

