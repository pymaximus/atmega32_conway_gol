/*
 *
 * General defines
 *
 */

#ifndef DEFINES_H_
#define DEFINES_H_

/* CPU frequency */
#define F_CPU 8000000UL

/* UART baud rate */
#define UART_BAUD  57600

#endif /* DEFINES_H_ */

