/****************************************************************************
 Title:    Sample unit test using minunit
 Author:   Frank Singleton
 File:     main.c
 Software: AVR-GCC 3.3
 Hardware: Atmega32 at 8 Mhz (Internal RC Osc). STK500v2

 Description:
 This example shows how use minunit

 *****************************************************************************/

#include "defines.h"    // Always First !!
#include "world.h"
#include "minunit.h"

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>
#include "uart.h"

// init UART stream
FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

void start_world(uint8_t *world) {
    // just for test
    clear_world(world);
    set_bit(world, 64, 32);
    set_bit(world, 65, 32);
    set_bit(world, 64, 33);
    set_bit(world, 65, 33);
}

int tests_run = 0;

int foo = 7;
int bar = 4;

static char * test_foo() {
    mu_assert("error, foo != 7", foo == 7);
    return 0;
}

static char * test_bar() {
    mu_assert("error, bar != 5", bar == 4);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_foo);
    mu_run_test(test_bar);
    return 0;
}

/*
 * Do all the startup-time peripheral initializations.
 */
static void ioinit(void) {
    uart_init();
    //adc_init();
}

//int main(int argc, char **argv) {
int main(void) {

    ioinit();
    DDRB = 0xFF;            // Configure PortB as output
    PORTB = 0xff;

    //PORTB |= (1 << PB0);
    PORTB &= ~(1 << PB0);

    // init stdout etc to our UART stream
    //stdout = stdin = stderr = &uart_str;

    //printf("Hello world!\n");

    /* Prepare to run all tests */
    char *result = all_tests();
    if (result != 0) {
        PORTB &= ~(1 << PB1);
        //printf("%s\n", result);
    } else {
        PORTB &= ~(1 << PB2);
        //printf("ALL TESTS PASSED\n");
    }
    PORTB &= ~(1 << PB3);
    //printf("Tests run: %d\n", tests_run);

    //return result != 0;

    while (true) {
        ;
    }
}

