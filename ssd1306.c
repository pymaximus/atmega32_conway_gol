
#include "ssd1306.h"
#include "i2cmaster.h"

void oled_init() {
    //i2c_init(SSD1306_DEFAULT_ADDRESS);
    i2c_init();

    // Turn display off
    oled_send_command(SSD1306_DISPLAYOFF);

    oled_send_command(SSD1306_SETDISPLAYCLOCKDIV);
    oled_send_command(0x80);

    oled_send_command(SSD1306_SETMULTIPLEX);
    oled_send_command(0x3F);

    oled_send_command(SSD1306_SETDISPLAYOFFSET);
    oled_send_command(0x00);

    oled_send_command(SSD1306_SETSTARTLINE | 0x00);

    // We use internal charge pump
    oled_send_command(SSD1306_CHARGEPUMP);
    oled_send_command(0x14);

    // Horizontal memory mode
    oled_send_command(SSD1306_MEMORYMODE);
    oled_send_command(0x00);

    oled_send_command(SSD1306_SEGREMAP | 0x1);

    oled_send_command(SSD1306_COMSCANDEC);

    oled_send_command(SSD1306_SETCOMPINS);
    oled_send_command(0x12);

    // Max contrast
    oled_send_command(SSD1306_SETCONTRAST);
    oled_send_command(0xCF);

    oled_send_command(SSD1306_SETPRECHARGE);
    oled_send_command(0xF1);

    oled_send_command(SSD1306_SETVCOMDETECT);
    oled_send_command(0x40);

    oled_send_command(SSD1306_DISPLAYALLON_RESUME);

    // Non-inverted display
    oled_send_command(SSD1306_NORMALDISPLAY);

    // Turn display back on
    oled_send_command(SSD1306_DISPLAYON);
}

//void sendCommand(uint8_t command) {
//    i2c_start();
//    i2c_write(0x00);
//    i2c_write(command);
//    i2c_stop();
//}

void oled_send_command(uint8_t command) {
    i2c_start(SSD1306_DEFAULT_ADDRESS + I2C_WRITE);
    i2c_write(0x00);
    i2c_write(command);
    i2c_stop();
}
void oled_invert(uint8_t inverted) {
    if (inverted) {
        oled_send_command(SSD1306_INVERTDISPLAY);
    } else {
        oled_send_command(SSD1306_NORMALDISPLAY);
    }
}

void oled_send_frame_buffer(const uint8_t * const buffer) {
    oled_send_command(SSD1306_COLUMNADDR);
    oled_send_command(0x00);
    oled_send_command(0x7F);

    oled_send_command(SSD1306_PAGEADDR);
    oled_send_command(0x00);
    oled_send_command(0x07);

    // We have to send the buffer as 16 bytes packets
    // Our buffer is 1024 bytes long, 1024/16 = 64
    // We have to send 64 packets
    for (uint8_t packet = 0; packet < 64; packet++) {
        i2c_start(SSD1306_DEFAULT_ADDRESS + I2C_WRITE);
        i2c_write(0x40);
        for (uint8_t packet_byte = 0; packet_byte < 16; ++packet_byte) {
            i2c_write(buffer[packet * 16 + packet_byte]);
        }
        i2c_stop();
    }
}
