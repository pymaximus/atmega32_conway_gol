/*
 * Basic frame buffer functions, based on lots of code from the internet.
 */

#include "frameBuffer.h"
#include "ssd1306.h"
#include <stdlib.h>
#include <string.h>

/**
 * Draw a bitmap from PROGMEM onto buffer in SRAM
 */
void fb_draw_bitmap(uint8_t * const buffer, const uint8_t * const progmem_bitmap, uint8_t height, uint8_t width, uint8_t pos_x,
        uint8_t pos_y) {
    uint8_t current_byte;
    uint8_t byte_width = (width + 7) / 8;

    for (uint8_t current_y = 0; current_y < height; current_y++) {
        for (uint8_t current_x = 0; current_x < width; current_x++) {
            current_byte = pgm_read_byte(progmem_bitmap + current_y * byte_width + current_x / 8);
            if (current_byte & (128 >> (current_x & 7))) {
                fb_set_pixel(buffer, current_x + pos_x, current_y + pos_y, 1);
            } else {
                fb_set_pixel(buffer, current_x + pos_x, current_y + pos_y, 0);
            }
        }
    }
}

/* For drawing widthx8 bitmap from PROGMEM to buffer
 * This is for fonts that are defined column wise to
 * make it faster to write out to buffer
 */
void fb_draw_bitmap_font8(uint8_t column, uint8_t page, const uint8_t * const progmem_bitmap, uint8_t width, uint8_t * const buff) {
    for (uint8_t i = 0; i < width; i++) {
        uint8_t current_byte = pgm_read_byte(&progmem_bitmap[i]);
        buff[i + ((page * 128) + column)] = current_byte;
    }
}

void fb_draw_bitmap_font8_letter(uint8_t column, uint8_t page, uint8_t letter, const uint8_t * const progmem_bitmap,
        uint8_t width, uint8_t * const buff) {
    if (letter < 32 || letter > 127) {
        return;
    }
    uint8_t offset = letter - 32;
    for (uint8_t i = 0; i < width; i++) {
        //uint8_t current_byte = pgm_read_byte(&progmem_bitmap[i]);
        uint8_t current_byte = pgm_read_byte(&progmem_bitmap[offset * width + i]);
        buff[i + ((page * 128) + column)] = current_byte;
    }
}

void fb_draw_bitmap_font8_string(uint8_t column, uint8_t page, const char * const string, const uint8_t * const progmem_bitmap,
        uint8_t width, uint8_t * const buff) {

    uint8_t i = 0;
    uint8_t alignment = 7;
    while (string[i] != 0) {
        fb_draw_bitmap_font8_letter(column + (alignment * i), page, string[i], progmem_bitmap, width, buff);
        i++;
    }
}

//void drawBuffer(const uint8_t *progmem_buffer) {
//    uint8_t current_byte;
//
//    for (uint8_t y_pos = 0; y_pos < 64; y_pos++) {
//        for (uint8_t x_pos = 0; x_pos < 128; x_pos++) {
//            current_byte = pgm_read_byte(progmem_buffer + y_pos * 16 + x_pos / 8);
//            if (current_byte & (128 >> (x_pos & 7))) {
//                drawPixel(progmem_buffer, x_pos, y_pos, 1);
//            } else {
//                drawPixel(progmem_buffer, x_pos, y_pos, 0);
//            }
//        }
//    }
//}

void fb_set_pixel(uint8_t * const buffer, uint8_t pos_x, uint8_t pos_y, uint8_t pixel_status) {
    if (pos_x >= SSD1306_WIDTH || pos_y >= SSD1306_HEIGHT) {
        return;
    }

    if (pixel_status) {
        buffer[pos_x + (pos_y / 8) * SSD1306_WIDTH] |= (1 << (pos_y & 7));
    } else {
        buffer[pos_x + (pos_y / 8) * SSD1306_WIDTH] &= ~(1 << (pos_y & 7));
    }
}

void fb_draw_pixel(uint8_t * const buffer, uint8_t pos_x, uint8_t pos_y) {
    if (pos_x >= SSD1306_WIDTH || pos_y >= SSD1306_HEIGHT) {
        return;
    }
    buffer[pos_x + (pos_y / 8) * SSD1306_WIDTH] |= (1 << (pos_y & 7));
}

void fb_draw_vline(uint8_t * const buffer, uint8_t x, uint8_t y, uint8_t length) {
    for (uint8_t i = 0; i < length; ++i) {
        fb_draw_pixel(buffer, x, i + y);
    }
}

void fb_draw_hLine(uint8_t * const buffer, uint8_t x, uint8_t y, uint8_t length) {
    for (uint8_t i = 0; i < length; ++i) {
        fb_draw_pixel(buffer, i + x, y);
    }
}

void fb_draw_rectangle(uint8_t * const buffer, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2) {
    uint8_t length = x2 - x1 + 1;
    uint8_t height = y2 - y1;

    fb_draw_hLine(buffer, x1, y1, length);
    fb_draw_hLine(buffer, x1, y2, length);
    fb_draw_vline(buffer, x1, y1, height);
    fb_draw_vline(buffer, x2, y1, height);
}

void fb_draw_rectangle_fill(uint8_t * const buffer, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t fill) {
    if (!fill) {
        fb_draw_rectangle(buffer, x1, y1, x2, y2);
    } else {
        uint8_t length = x2 - x1 + 1;
        uint8_t height = y2 - y1;

        for (int x = 0; x < length; ++x) {
            for (int y = 0; y <= height; ++y) {
                fb_draw_pixel(buffer, x1 + x, y + y1);
            }
        }
    }
}

void fb_clear(uint8_t * const buffer) {
    for (uint16_t buffer_location = 0; buffer_location < SSD1306_BUFFERSIZE; buffer_location++) {
        buffer[buffer_location] = 0x00;
    }
}

void fb_fill(uint8_t * const buffer, uint8_t fill) {
    for (uint16_t buffer_location = 0; buffer_location < SSD1306_BUFFERSIZE; buffer_location++) {
        buffer[buffer_location] = fill;
    }
}

void fb_memset_fill(uint8_t * const buffer, uint8_t fill) {
    memset(buffer, fill, SSD1306_BUFFERSIZE);
}

void fb_invert(uint8_t status) {
    /* use hardware invert on oled */
    oled_invert(status);
}

void fb_show(const uint8_t * const buffer) {
    oled_send_frame_buffer(buffer);
}

/**
 * Bresenham's line draw
 */
void fb_draw_line(uint8_t *buff, int16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
    int16_t dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
    int16_t dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
    int16_t err = dx + dy, e2; /* error value e_xy */

    for (;;) { /* loop */
        fb_draw_pixel(buff, x0, y0);
        if (x0 == x1 && y0 == y1)
            break;
        e2 = 2 * err;
        if (e2 >= dy) {
            err += dy;
            x0 += sx;
        } /* e_xy+e_x > 0 */
        if (e2 <= dx) {
            err += dx;
            y0 += sy;
        } /* e_xy+e_y < 0 */
    }
}
