/****************************************************************************
 Title:    Sample unit test using minunit
 Author:   Frank Singleton
 File:     main.c
 Software: AVR-GCC 3.3
 Hardware: Atmega32 at 8 Mhz (Internal RC Osc). STK500v2

 Description:
 This example shows how use minunit

 *****************************************************************************/

#include "defines.h"    // Always First !!
#include "world.h"
#include "minunit.h"

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>
#include "uart.h"
#include <util/delay.h>

// init UART stream
FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

extern uint8_t world_current_gen[];
extern uint8_t world_next_gen[];

void start_world(uint8_t *world) {
    // just for test
    clear_world(world);
    set_bit(world, 64, 32);
    set_bit(world, 65, 32);
    set_bit(world, 64, 33);
    set_bit(world, 65, 33);
}

int tests_run = 0;

int foo = 7;
int bar = 4;

static char * test_clear_world_current_gen() {
    clear_world(world_current_gen);
    mu_assert("error, foo != 7", world_current_gen[0] == 0);
    return 0;
}

static char * test_clear_world_current_gen1() {
    world_current_gen[0] = 1;
    mu_assert("error, foo != 7", world_current_gen[0] == 1);
    clear_world(world_current_gen);
    mu_assert("error, foo != 7", world_current_gen[0] == 0);
    return 0;
}

static char * test_clear_world_next_gen() {
    clear_world(world_next_gen);
    mu_assert("error, foo != 7", world_next_gen[0] == 0);
    return 0;
}

static char * test_clear_world_next_gen1() {
    world_next_gen[0] = 1;
    mu_assert("error, foo != 7", world_next_gen[0] == 1);
    clear_world(world_next_gen);
    mu_assert("error, foo != 7", world_next_gen[0] == 0);
    return 0;
}

static char * test_set_bit_0_0_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 0);
    mu_assert("error, foo != 7", world_current_gen[0] == 0x80);
    return 0;
}

static char * test_set_bit_1_0_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 1, 0);
    mu_assert("error, foo != 7", world_current_gen[0] == 0x40);
    return 0;
}

static char * test_set_bit_8_0_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 8, 0);
    mu_assert("error, foo != 7", world_current_gen[1] == 0x80);
    return 0;
}

static char * test_set_bit_0_1_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 1);
    mu_assert("error, foo != 7", world_current_gen[4] == 0x80);
    return 0;

}

static char * test_set_bit_0_2_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 2);
    mu_assert("error, foo != 7", world_current_gen[WORLD_WIDTH * 2 / 8] == 0x80);
    return 0;

}
static char * test_set_bit_31_31_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 31, 31);
    mu_assert("error, foo != 7", world_current_gen[127] == 0x01);
    return 0;
}

static char * test_set_bit_test_bit_0_0_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 0);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 0, 0) == 1);
    return 0;
}

static char * test_set_bit_test_bit_3_0_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 3, 0);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 3, 0) == 1);
    mu_assert("error, foo != 7", world_current_gen[0] == 0x10);
    return 0;
}

static char * test_set_bit_test_bit_5_8_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 5, 8);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 5, 8) == 1);
    mu_assert("error, foo != 7", world_current_gen[32] == 0x04);
    return 0;
}

static char * test_illegal_set_bit_test_bit_40_40_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 40, 40);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 40, 40) == 2);
    return 0;
}

static char * test_count_neighbors_0() {
    clear_world(world_current_gen);
    mu_assert("error, foo != 7", count_neighbors(world_current_gen, 1, 1) == 0);
    return 0;
}

static char * test_count_neighbors_1() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 0);
    mu_assert("error, foo != 7", count_neighbors(world_current_gen, 1, 1) == 1);
    return 0;
}

static char * test_count_neighbors_3() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 0);
    set_bit(world_current_gen, 0, 1);
    set_bit(world_current_gen, 0, 2);
    mu_assert("error, foo != 7", count_neighbors(world_current_gen, 1, 1) == 3);
    return 0;
}

static char * test_count_neighbors_8() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 0, 0);
    set_bit(world_current_gen, 0, 1);
    set_bit(world_current_gen, 0, 2);

    set_bit(world_current_gen, 2, 0);
    set_bit(world_current_gen, 2, 1);
    set_bit(world_current_gen, 2, 2);

    set_bit(world_current_gen, 1, 0);
    set_bit(world_current_gen, 1, 2);

    mu_assert("error, foo != 7", count_neighbors(world_current_gen, 1, 1) == 8);
    return 0;
}

static char * test_evolve() {
    clear_world(world_current_gen);
    clear_world(world_next_gen);
    set_bit(world_current_gen, 5, 5);
    set_bit(world_current_gen, 6, 5);
    set_bit(world_current_gen, 7, 5);

    evolve_world(world_current_gen, world_next_gen);

    mu_assert("error, foo != 7", test_bit(world_next_gen, 5, 5) == 0);
    mu_assert("error, foo != 7", test_bit(world_next_gen, 7, 5) == 0);

    mu_assert("error, foo != 7", test_bit(world_next_gen, 6, 4) == 1);
    mu_assert("error, foo != 7", test_bit(world_next_gen, 6, 5) == 1);
    mu_assert("error, foo != 7", test_bit(world_next_gen, 6, 6) == 1);

    return 0;
}

static char * test_set_bit_test_bit_all_world_current_gen() {
    clear_world(world_current_gen);
    for (uint8_t x = 0; x < WORLD_WIDTH; x++) {
        for (uint8_t y = 0; y < WORLD_HEIGHT; y++) {
            set_bit(world_current_gen, x, y);
            mu_assert("error, foo != 7", test_bit(world_current_gen, x, y) == 1);
            clear_bit(world_current_gen, x, y);
            mu_assert("error, foo != 7", test_bit(world_current_gen, x, y) == 0);
        }
    }
    return 0;
}

static char * test_set_bit_test_bit_clear_bit_test_bit_world_current_gen() {
    clear_world(world_current_gen);
    set_bit(world_current_gen, 8, 9);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 8, 9) == 1);
    clear_bit(world_current_gen, 8, 9);
    mu_assert("error, foo != 7", test_bit(world_current_gen, 8, 9) == 0);

    return 0;
}

static char * test_bar() {
    mu_assert("error, bar != 5", bar == 4);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_clear_world_current_gen);
    mu_run_test(test_clear_world_current_gen1);
    mu_run_test(test_clear_world_next_gen);
    mu_run_test(test_clear_world_next_gen1);
    mu_run_test(test_set_bit_0_0_world_current_gen);
    mu_run_test(test_set_bit_1_0_world_current_gen);
    mu_run_test(test_set_bit_8_0_world_current_gen);
    mu_run_test(test_set_bit_0_1_world_current_gen);
    mu_run_test(test_set_bit_0_2_world_current_gen);
    mu_run_test(test_set_bit_31_31_world_current_gen);
    mu_run_test(test_set_bit_test_bit_0_0_world_current_gen);
    mu_run_test(test_set_bit_test_bit_3_0_world_current_gen);
    mu_run_test(test_set_bit_test_bit_5_8_world_current_gen);
    mu_run_test(test_illegal_set_bit_test_bit_40_40_world_current_gen);
    mu_run_test(test_count_neighbors_0);
    mu_run_test(test_count_neighbors_1);
    mu_run_test(test_count_neighbors_3);
    mu_run_test(test_count_neighbors_8);
    mu_run_test(test_set_bit_test_bit_all_world_current_gen);
    mu_run_test(test_set_bit_test_bit_clear_bit_test_bit_world_current_gen);
    mu_run_test(test_evolve);

    //mu_run_test(test_bar);
    return 0;
}

/*
 * Do all the startup-time peripheral initializations.
 */
static void ioinit(void) {
    uart_init();
    //adc_init();
}

//int main(int argc, char **argv) {
int main(void) {

    ioinit();
    DDRB = 0xFF;            // Configure PortB as output
    PORTB = 0xff;

    //PORTB |= (1 << PB0);
    PORTB &= ~(1 << PB0);

    // init stdout etc to our UART stream
    stdout = stdin = stderr = &uart_str;

    //printf("Hello world!\n");

    /* Prepare to run all tests */
    char *result = all_tests();
    _delay_ms(5000);
    if (result != 0) {
        PORTB &= ~(1 << PB1);
        printf("%s\n", result);
    } else {
        PORTB &= ~(1 << PB2);
        printf("ALL TESTS PASSED\n");
    }
    PORTB &= ~(1 << PB3);
    printf("Tests run: %d\n", tests_run);

    return result != 0;

//    while (true) {
//        ;
//    }
}

